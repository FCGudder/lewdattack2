export const LEWDATTACK = {};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 LEWDATTACK.stats = {
  "strength": "LEWDATTACK.stat.strength",
  "body": "LEWDATTACK.stat.body",
  "agility": "LEWDATTACK.stat.agility",
  "endurance": "LEWDATTACK.stat.endurance",
  "dexterity": "LEWDATTACK.stat.dexterity",
  "perception": "LEWDATTACK.stat.perception",
  "willpower": "LEWDATTACK.stat.willpower",
  "intelligence": "LEWDATTACK.stat.intelligence",
  "charisma": "LEWDATTACK.stat.charisma",
  "magic": "LEWDATTACK.stat.magic"
};

LEWDATTACK.statAbbreviations = {
  "strength": "LEWDATTACK.stat.strength_abbr",
  "body": "LEWDATTACK.stat.body_abbr",
  "agility": "LEWDATTACK.stat.agility_abbr",
  "endurance": "LEWDATTACK.stat.endurance_abbr",
  "dexterity": "LEWDATTACK.stat.dexterity_abbr",
  "perception": "LEWDATTACK.stat.perception_abbr",
  "willpower": "LEWDATTACK.stat.willpower_abbr",
  "intelligence": "LEWDATTACK.stat.intelligence_abbr",
  "charisma": "LEWDATTACK.stat.charisma_abbr",
  "magic": "LEWDATTACK.stat.magic_abbr"
};